/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author giovanna
 */
@Stateless
public class BiblFacade extends AbstractFacade<Bibl> implements BiblFacadeLocal {
    @PersistenceContext(unitName = "Spike1415-1-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BiblFacade() {
        super(Bibl.class);
    }
    
}
