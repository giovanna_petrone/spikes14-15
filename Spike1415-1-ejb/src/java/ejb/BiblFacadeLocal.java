/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author giovanna
 */
@Local
public interface BiblFacadeLocal {

    void create(Bibl bibl);

    void edit(Bibl bibl);

    void remove(Bibl bibl);

    Bibl find(Object id);

    List<Bibl> findAll();

    List<Bibl> findRange(int[] range);

    int count();
    
}
