/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author giovanna
 */
@WebService(serviceName = "NewWebService")
@Stateless()
public class NewWebService {
    @EJB
    private GestoreBiblioteca ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "aggiungiLibro")
    @Oneway
    public void aggiungiLibro(@WebParam(name = "titolo") String titolo, @WebParam(name = "autore") String autore) {
        ejbRef.aggiungiLibro(titolo, autore);
    }

    @WebMethod(operationName = "getLibri")
    public List<Libro> getLibri() {
        return ejbRef.getLibri();
    }
    
}
