<-- 
    Document   : bibliojsp
    Created on : Sep 4, 2014, 9:51:37 AM
    Author     : giovanna
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="include/css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="include/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="include/css/main.css">

        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>   
        <%@ include file="headerStandardPage.jsp" %>     
        
        <div class="jumbotron">
            <div class="container">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Indice</th>
                            <th>Titolo</th>
                            <th>Autore</th>
                            <th>Anno di Pubblicazione</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="active">
                            <td>1</td>
                            <td>ff</td>
                            <td>04/07/2014</td>
                            <td>Call in to confirm</td>
                        </tr>
                        <tr class="success">
                            <td>2</td>
                            <td>Water</td>
                            <td>01/07/2014</td>
                            <td>Paid</td>
                        </tr>
                        <tr class="info">
                            <td>3</td>
                            <td>Internet</td>
                            <td>05/07/2014</td>
                            <td>Change plan</td>
                        </tr>
                        <tr class="warning">
                            <td>4</td>
                            <td>Electricity</td>
                            <td>03/07/2014</td>
                            <td>Pending</td>
                        </tr>
                        <tr class="danger">
                            <td>5</td>
                            <td>Telephone</td>
                            <td>06/07/2014</td>
                            <td>Due</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <%@ include file="footer.jsp" %>
    </body>
</html>

